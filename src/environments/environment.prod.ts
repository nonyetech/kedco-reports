// export const environment = {
//   production: false
// };
export const environment = {
	production: true,
	useMocks: false,
	api: {
		baseUrl: 'http://197.253.19.76:6200/api/v1/kedco',
		regions: ['KANO INDUSTRIAL REGION', 'KATSINA CENTRAL REGION'],
		csps: ['30/0 UNDERTAKING', 'SHARADA UNDERTAKING']
	}
  };