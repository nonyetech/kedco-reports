import {
  NO_ERRORS_SCHEMA,
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDBBootstrapModulesPro } from "ng-uikit-pro-standard";
import { ServiceStatusComponent } from "./status/service-status/service-status.component";
import { BvnStatusComponent } from "./status/bvn-status/bvn-status.component";
import { SummaryService } from "app/_service/summary.service";
import { CspService } from "app/_service/csp.service";
import { ProductsService } from "app/_service/products.service";
import { PaymentMethodService } from "app/_service/payment-method.service";
import { TransactionService } from "app/_service/transaction.service";
import { LoginComponent } from "app/_auth/login/login.component";
import { AdminDashboardComponent } from "./admin/admin-dashboard/admin-dashboard.component";
import { TodayCardComponent } from "./admin/dashboard-cards/daily-cards/today-card/today-card.component";
import { ThisMonthCardComponent } from "./admin/dashboard-cards/monthly-cards/this-month-card/this-month-card.component";
import { ThisWeekCardComponent } from "./admin/dashboard-cards/weekly-cards/this-week-card/this-week-card.component";
import { TransactionComponent } from "./Transaction/transaction/transaction.component";
import { ModelComponent } from "./Transaction/transaction/model/model.component";
import { SharedModule } from "app/shared/shared.module";
import { OrderModule } from "ngx-order-pipe";
import { PaginationModule } from "ngx-bootstrap";
import { TransactionReversalComponent } from "./Transaction/transaction-reversal/transaction-reversal.component";
import { ReversalModelComponent } from "./Transaction/transaction-reversal/reversal-model/reversal-model.component";
import { TransactionRemittanceComponent } from "./Transaction/transaction-remittance/transaction-remittance.component";
import { SummaryReportComponent } from "./Transaction/summary-report/summary-report.component";
import { RegisterComponent } from "app/_auth/register/register.component";
import { RoleManagementComponent } from "./admin/role-management/role-management.component";
import { UserModalComponent } from "./admin/role-management/user-modal/user-modal.component";
import { HasRoleDirective } from "app/main-layout/navigation/directives/has-role.directive";
import { NavigationComponent } from "../main-layout/navigation/navigation.component";
import { FooterComponent } from "../main-layout/footer/footer.component";
import { NavbarComponent } from "../main-layout/navigation/navbar/navbar.component";
import { DirectiveModule } from "app/directive.module";
import { DayCardComponent } from "./admin/dashboard-cards/daily-cards/day-card/day-card.component";
import { MonthCardComponent } from "./admin/dashboard-cards/monthly-cards/month-card/month-card.component";
import { WeekCardComponent } from "./admin/dashboard-cards/weekly-cards/week-card/week-card.component";
import { RouterModule } from "@angular/router";
import { AppRoutes } from "app/app.routes.service";
import { ExcelService } from "app/_service/excel.service";
import { McashTransactionComponent } from "./Mcash/transaction/mcash-transaction.component";
import { McashComponent } from "./Mcash/terminals/mcash.component";
import { ErrorAnalysisComponent } from "./error-analysis/error-analysis.component";
import { StatusModalComponent } from "./status/service-status/status-modal/status-modal.component";
import { KedcoDayCardComponent } from "./admin/kedco-dashboard-cards/daily-cards/day-card/day-card.component";
import { KedcoMonthCardComponent } from "./admin/kedco-dashboard-cards/monthly-cards/month-card/month-card.component";
import { KedcoWeekCardComponent } from "./admin/kedco-dashboard-cards/weekly-cards/week-card/week-card.component";

import { KedcoTodayCardComponent } from "./admin/kedco-dashboard-cards/daily-cards/today-card/today-card.component";
import { KedcoThisMonthCardComponent } from "./admin/kedco-dashboard-cards/monthly-cards/this-month-card/this-month-card.component";
import { KedcoThisWeekCardComponent } from "./admin/kedco-dashboard-cards/weekly-cards/this-week-card/this-week-card.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutes,
    RouterModule,
    SharedModule,
    DirectiveModule,
    OrderModule,
    MDBBootstrapModulesPro.forRoot(),
    PaginationModule.forRoot()
  ],
  declarations: [
    // Itex Copmponent
    TodayCardComponent,
    ThisMonthCardComponent,
    ThisWeekCardComponent,

    KedcoTodayCardComponent,
    KedcoThisMonthCardComponent,
    KedcoThisWeekCardComponent,

    ServiceStatusComponent,
    BvnStatusComponent,

    LoginComponent,
    RegisterComponent,
    AdminDashboardComponent,
    TransactionComponent,
    ModelComponent,
    TransactionReversalComponent,
    ReversalModelComponent,
    TransactionRemittanceComponent,
    SummaryReportComponent,
    RoleManagementComponent,
    UserModalComponent,
    DayCardComponent,
    MonthCardComponent,
    WeekCardComponent,
    KedcoDayCardComponent,
    KedcoMonthCardComponent,
    KedcoWeekCardComponent,

    McashTransactionComponent,
    McashComponent,
    ErrorAnalysisComponent,
    StatusModalComponent
  ],
  exports: [
    MDBBootstrapModulesPro,

    TodayCardComponent,
    ThisMonthCardComponent,
    ThisWeekCardComponent,

    KedcoTodayCardComponent,
    KedcoThisMonthCardComponent,
    KedcoThisWeekCardComponent,
    
    ServiceStatusComponent,
    BvnStatusComponent,
    LoginComponent,
    RegisterComponent,
    AdminDashboardComponent,
    TransactionComponent,
    ModelComponent,
    TransactionReversalComponent,
    ReversalModelComponent,
    TransactionRemittanceComponent,
    SummaryReportComponent,
    RoleManagementComponent,
    DayCardComponent,
    MonthCardComponent,
    WeekCardComponent,
    KedcoDayCardComponent,
    KedcoMonthCardComponent,
    KedcoWeekCardComponent,

    McashTransactionComponent,
    McashComponent
  ],
  providers: [
    // itex
    SummaryService,
    CspService,
    ProductsService,
    PaymentMethodService,
    TransactionService,
    ExcelService
  ],

  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
})
export class PagesModule {}
