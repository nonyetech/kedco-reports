import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionRemittanceComponent } from './transaction-remittance.component';

describe('TransactionLocksComponent', () => {
  let component: TransactionRemittanceComponent;
  let fixture: ComponentFixture<TransactionRemittanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionRemittanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionRemittanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
