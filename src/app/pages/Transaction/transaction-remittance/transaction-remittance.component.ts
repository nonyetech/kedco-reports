import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TransactionService } from 'app/_service/transaction.service';
import { Router } from '@angular/router';
import { IMyOptions, ToastService } from 'ng-uikit-pro-standard';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { environment } from 'environments/environment';
import { KedcosummaryService } from 'app/_service/kedcosummary.service';
import { AuthService } from 'app/_auth/auth.service';

@Component({
  selector: 'app-transaction-remittance',
  templateUrl: './transaction-remittance.component.html',
  styleUrls: ['./transaction-remittance.component.scss']
})
export class TransactionRemittanceComponent implements OnInit {
  isData: boolean;
  loading = true;
  remittanceData: any;
  data: any;
  tranLockAmount: any;
  // pagination
  perPage = 50;
  currentPage = 1;
  lastPage: number;
  serial: number;
  maxSize = 10;
  // filters
  searchForm: FormGroup;
  filter: any;
  filterValue: any;
  transactionReference: any;
  walletId: any;
  filterData: any;
  start: any;
  end: any;
  operator: any;
  csp: any;
  region: any;
  range: any;
  modalRef: BsModalRef;
  usersOptionSelect: any;

  DateObj: any = new Date();
  dateRange = (String)(this.DateObj.getFullYear() + '/' + (this.DateObj.getMonth() + 1) + '/' + this.DateObj.getDate());
  newRange = `${this.dateRange} - ${this.dateRange}`;
  payload = {
    "date": this.newRange,
    "operator": "",
    "csp": "",
    "region": "",
    // "download": false,
  };


  constructor(private transactionService: TransactionService,
    private router: Router, private fb: FormBuilder,
    private modalService: BsModalService, private toastService: ToastService, 
    private summaryService: KedcosummaryService,
    private authService: AuthService) {
    this.searchForm = this.fb.group({
      method: ['', Validators.min],
      startDate: ['', Validators.min],
      endDate: ['', Validators.min],
      filterValue: ['',]
    });
  }

  ngOnInit() {
    this.RemittanceReport(this.payload);
    this.getUsers();
  }

  RemittanceReport(payload) {
    this.isData = true;
    this.loading = true;
    this.summaryService.getRemittanceReport(payload).subscribe((data) => {
      this.loading = false;
      console.log(data);
      this.remittanceData = data;
    }, error => {
      this.isData = false;
      this.loading = false;
      console.log('cant get transaction locks', error);
    })
  };

  getCsp(event) {
    this.csp = event.target.value;
  }
  getRegion(event) {
    this.region = event.target.value;
  }
  getOperator(event) {
    this.operator = event.target.value;
  }
 

  getUsers() {
    this.isData = true;
    this.loading = true;
    this.authService.getUsers().subscribe((data: any) => {
      this.loading = false;
      let users = data.data;
      let UserData = [];
      users.forEach(function(item){
        let obj = { value: item.username, label: item.name }
        UserData.push(obj); 
      });    
      this.usersOptionSelect = UserData
      console.log(this.usersOptionSelect);
    }, error => {
      this.isData = false;
      this.loading = false
      console.log('cant get users- ', error);

    });
  }

  regions = environment.api.regions;
  csps = environment.api.csps;


  // pageChanged(event: any): void {
  //   this.loading = true;
  //   this.currentPage = event.page;
  //   this.TransactionLocks(this.payload);
  //   this.router.navigateByUrl('/transaction/locks', { queryParams: { page: this.currentPage } });
  // };

  // filters
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy/mm/dd',
    startDate: this.dateRange,
    ariaLabelOpenCalendar: 'Open Calendar',
    closeAfterSelect: true,
    minYear: 1900,
    // disableUntil:
    //   { year: this.DateObj.getFullYear(), month: this.DateObj.getMonth(), day: this.DateObj.getDate() }
  };

  searchTrans() {
    this.start = this.searchForm.value.startDate;
    this.end = this.searchForm.value.endDate;
    this.range = `${this.start} - ${this.end}`;
    this.filterData = {
      "date": this.range,
      "operator": this.operator ? this.operator : "",
      "csp": this.csp ? this.csp: "",
      "region": this.region ? this.region : "",
    };
    console.log(this.filterData);
    this.RemittanceReport(this.filterData);
  }

  getFilter() {
    this.filterValue = this.searchForm.value.filterValue;
    if (this.filter == 'Agent ID') {
      this.walletId = this.filterValue;
    } else if (this.filter == 'Transaction Ref') {
      this.transactionReference = this.filterValue;
    }
  }

}
