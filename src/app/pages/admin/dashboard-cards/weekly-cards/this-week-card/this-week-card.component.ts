import { Component, OnInit, OnDestroy } from '@angular/core';
import { SummaryService } from 'app/_service/summary.service';
import * as math from 'mathjs';
import { SubSink } from 'subsink/dist/subsink';
import { Subscription, Observable } from 'rxjs';


@Component({
  selector: 'app-this-week-card',
  templateUrl: './this-week-card.component.html',
  styleUrls: ['./this-week-card.component.scss']
})
export class ThisWeekCardComponent implements OnInit, OnDestroy {

  loading = false;
  isData: boolean;
  refresh: Subscription;

  //version 2 variables
  
  //response holder for success and  fail 
  responseCurrent: any;
  responsePrevious: any;

  previousTotal: any = null;

  //response holder for success and  fail count
  successCountCurrent: any;
  failCountCurrent: any;

  //response holder for success and  fail  amount
  successAmountCurrent: any = null;
  failAmountCurrent: any = null;

  //percentage change
  percentChange: any = null;

  //response holder for success and  fail  percentage
  successPercentCurrent: any;
  failPercentCurrent: any;

  //total amount and count
  totalAmountCurrent: any;
  totalCountCurrent: any;

  //previous data
  previousAmountSuccess:any;
  previousCountSuccess:any;

  previousAmountFailed:any;
  previousCountFailed:any;

  previousPercentFailed:any;
  previousPercentSuccess:any;

  totalCountPrevious:any;


  
  constructor(private summaryService: SummaryService) { }

  async ngOnInit() {
    
  }
  

  ngOnDestroy() {
    this.refresh.unsubscribe();
  }

}