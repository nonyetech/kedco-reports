import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KedcoTodayCardComponent } from './today-card.component';

describe('TodayCardComponent', () => {
  let component: KedcoTodayCardComponent;
  let fixture: ComponentFixture<KedcoTodayCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KedcoTodayCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KedcoTodayCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
