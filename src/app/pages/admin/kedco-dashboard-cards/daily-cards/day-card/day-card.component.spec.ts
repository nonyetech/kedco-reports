import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KedcoDayCardComponent } from './day-card.component';

describe('DayCardComponent', () => {
  let component: KedcoDayCardComponent;
  let fixture: ComponentFixture<KedcoDayCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KedcoDayCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KedcoDayCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
