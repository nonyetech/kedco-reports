import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KedcoMonthCardComponent } from './month-card.component';

describe('KedcoMonthCardComponent', () => {
  let component: KedcoMonthCardComponent;
  let fixture: ComponentFixture<KedcoMonthCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KedcoMonthCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KedcoMonthCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
