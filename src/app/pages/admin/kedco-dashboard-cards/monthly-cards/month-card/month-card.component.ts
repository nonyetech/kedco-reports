import { Component, OnInit } from '@angular/core';
import { KedcosummaryService } from 'app/_service/kedcosummary.service';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { IMyOptions } from 'ng-uikit-pro-standard';
import { CspService } from 'app/_service/csp.service';
import { environment } from 'environments/environment';
import { ExcelService } from 'app/_service/excel.service';


@Component({
  selector: 'app-month-card',
  templateUrl: './month-card.component.html',
  styleUrls: ['./month-card.component.scss']
})


export class KedcoMonthCardComponent implements OnInit {
  loading = false;
  isData: boolean;
  refresh: Subscription;

  //response holder for success and  fail 
  responseCurrent: any;
  responsePrevious: any;

  previousTotal: any = null;
  //response holder for success and  fail count
  successCountCurrent: any;
  failCountCurrent: any;

  //response holder for success and  fail  amount
  successAmountCurrent: any = null;
  failAmountCurrent: any = null;

  //percentage change
  percentChange: any = null;

  //response holder for success and  fail  percentage
  successPercentCurrent: any;
  failPercentCurrent: any;

  //total amount and count
  totalAmountCurrent: any;
  totalCountCurrent: any;

  //previous data
  previousAmountSuccess: any;
  previousCountSuccess: any;

  previousAmountFailed: any;
  previousCountFailed: any;

  previousPercentFailed: any;
  previousPercentSuccess: any;

  totalCountPrevious: any;

  csp: any;
  region: any;
  start: any;
  end: any;
  range: any;
  searchForm: any;
  payload: any;
  operator: any;
  DateObj: any = new Date();
  defaultRange = (String)(this.DateObj.getFullYear() + '/' + (this.DateObj.getMonth() + 1) + '/' + this.DateObj.getDate());
  hideDashboardText = false;

  outputChannel: any;
  elementsTopCsp: any;
  elementsTopRegions: any;
  elementsTopCashiers: any;
  elementsTopSalesReps: any;
  Url: any;

  thisMonthPayload = {
      "category": "kedco",
      "date": "month",
      "dateRange": "",
      "region": this.region ? this.region : "",
      "csp": this.csp ? this.csp : "",
      "operator": this.operator ? this.operator : ""
  }

  lastMonthPayload = {
      "category": "kedco",
      "date": "last_month",
      "dateRange": "",
      "region": this.region ? this.region : "",
      "csp": this.csp ? this.csp : "",
      "operator": this.operator ? this.operator : ""
  }

  constructor(private summaryService: KedcosummaryService, private fb: FormBuilder,
    private cspService: CspService,
    private excelService: ExcelService) {
    this.searchForm = this.fb.group({
      method: ['', Validators.min],
      startDate: ['', Validators.min],
      endDate: ['', Validators.min],
      filterValue: ['',]
    });
  }

  async ngOnInit() {
    await this.getTopFiveCsp();
    await this.getTopFiveRegions();
    await this.getTopFiveCashiers();
    await this.getTopFiveSalesReps();
    this.refresh = Observable.interval(15 * 60 * 1000).subscribe(() => {
      this.getThisMonthTransaction();
    })
    await this.getThisMonthTransaction();
  }

  getSummaryReportXlsx(){
    this.isData = true;
    this.loading = true,
    this.start = this.searchForm.value.startDate;
    this.end = this.searchForm.value.endDate;
    this.range = `${this.start} - ${this.end}`;
    let payload = {
      'date': this.range ? this.range : "",
    }
    this.summaryService.getSummary(payload).subscribe(responseData => {
      let RegionSummary = (responseData[0].data) ? responseData[0].data : [];
      let TariffSummary = (responseData[1].data) ? responseData[1].data : [];
      let CspSummary = (responseData[2].data) ? responseData[2].data : [];
      let SalesRepSummary = (responseData[3].data) ? responseData[3].data : [];;
      this.loading = false;
      this.excelService.exportMultipleAsExcelFile(RegionSummary, TariffSummary, CspSummary, SalesRepSummary, 'ITEX-TranReport');
      
    }, error => {
      this.isData = false;
      this.loading = false;
      console.log('cant get today response', error);
    });

  }
  /**
   * Array blocks for Selects on the Dashboard
   */
  regions = environment.api.regions;
  csps = environment.api.csps;
  headElements = ['Names', 'Total (N)'];

  getCsp(event) {
    this.csp = event.target.value;
  }

  getRegion(event) {
    this.region = event.target.value;
  }

  getTopFiveCsp() {
    this.isData = true;
    this.loading = true,
    this.Url = environment.api.baseUrl + '/transaction/top5/csp/kedco/month';
      this.cspService.getChannel(this.Url).subscribe(responseData => {
        this.loading = false;
        this.outputChannel = responseData.data.response;
        this.outputChannel = this.outputChannel.sort((a, b) => (a.total > b.total) ? -1 : 1);
        //splice the array and pick the top five
        let sortArray = this.outputChannel;
        this.elementsTopCsp = sortArray.splice(0, 5);
      }, error => {
        this.isData = false;
        this.loading = false;
        console.log('cant get today response', error);
      });
  }

  getTopFiveRegions() {
    this.isData = true;
    this.loading = true,
    this.Url = environment.api.baseUrl + '/transaction/top5/region/kedco/month';
      this.cspService.getChannel(this.Url).subscribe(responseData => {
        this.loading = false;
        this.outputChannel = responseData.data.response;
        this.outputChannel = this.outputChannel.sort((a, b) => (a.total > b.total) ? -1 : 1);
        //splice the array and pick the top five
        let sortArray = this.outputChannel;
        this.elementsTopRegions = sortArray.splice(0, 5);
        
      }, error => {
        this.isData = false;
        this.loading = false;
        console.log('cant get today response', error);
      });
  }

  getTopFiveCashiers() {
    this.isData = true;
    this.loading = true,
    this.Url = environment.api.baseUrl + '/transaction/top5/cashiers/kedco/month';
      this.cspService.getChannel(this.Url).subscribe(responseData => {
        this.loading = false;
        this.outputChannel = responseData.data.response;
        this.outputChannel = this.outputChannel.sort((a, b) => (a.total > b.total) ? -1 : 1);
        //splice the array and pick the top five
        let sortArray = this.outputChannel;
        this.elementsTopCashiers = sortArray.splice(0, 5);        
      }, error => {
        this.isData = false;
        this.loading = false;
        console.log('cant get today response', error);
      });
  }

  getTopFiveSalesReps() {
    this.isData = true;
    this.loading = true,
    this.Url = environment.api.baseUrl + '/transaction/top5/salesrep/kedco/month';
      this.cspService.getChannel(this.Url).subscribe(responseData => {
        this.loading = false;
        this.outputChannel = responseData.data.response;
        this.outputChannel = this.outputChannel.sort((a, b) => (a.total > b.total) ? -1 : 1);
        //splice the array and pick the top five
        let sortArray = this.outputChannel;
        this.elementsTopSalesReps = sortArray.splice(0, 5);
      }, error => {
        this.isData = false;
        this.loading = false;
        console.log('cant get today response', error);
      });
  }

  // filters
  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy/mm/dd',
    startDate: this.defaultRange,
    ariaLabelOpenCalendar: 'Open Calendar',
    closeAfterSelect: true,
    minYear: 1900,
    // disableUntil:
    //   { year: this.DateObj.getFullYear(), month: this.DateObj.getMonth(), day: this.DateObj.getDate() }
  };

  summarySearch() {
    this.hideDashboardText = true;
    this.start = this.searchForm.value.startDate;
    this.end = this.searchForm.value.endDate;
    this.range = `${this.start} - ${this.end}`;
    this.thisMonthPayload = {
      "category": "kedco",
      "date": "",
      "dateRange": this.range ? this.range : "",
      "region": this.region ? this.region : "",
      "csp": this.csp ? this.csp : "",
      "operator": this.operator ? this.operator : ""
    }

    this.lastMonthPayload = {
      "category": "kedco",
      "date": "",
      "dateRange": this.range ? this.range : "",
      "region": this.region ? this.region : "",
      "csp": this.csp ? this.csp : "",
      "operator": this.operator ? this.operator : ""
    }
    this.getThisMonthTransaction();

  }

  ngOnDestroy() {
    this.refresh.unsubscribe();
  }
  
  getThisMonthTransaction() {
    this.isData = true;
    this.loading = true,
      this.summaryService.getThisMonth(this.thisMonthPayload, this.lastMonthPayload).subscribe(responseList => {
          
          console.log(responseList); 
          this.loading = false;  this.responseCurrent = responseList[0];
          this.responsePrevious = responseList[1];
    
          // console.log(this.responsePrevious);
    
          //summary of the data for success and fail
          this.successCountCurrent = parseInt(this.responseCurrent.data.successfulCount);
          this.failCountCurrent = parseInt(this.responseCurrent.data.failedCount);
    
          //summary of the data for success and fail
          this.successAmountCurrent = parseFloat(this.responseCurrent.data.successfulAmount);
          this.failAmountCurrent = parseFloat(this.responseCurrent.data.failedAmount);
    
          //output response to display
          this.totalCountCurrent = this.responseCurrent.data.transactionCount;
          this.totalAmountCurrent = this.responseCurrent.data.totalAmount;
    
    
          //summary of the data for success and fail
          this.successPercentCurrent = this.responseCurrent.data.successfulPercent;
          this.failPercentCurrent = this.responseCurrent.data.failedPercent;
    
          //summary of the data for previous  success and fail
    
          this.previousAmountSuccess = parseFloat(this.responsePrevious.data.successfulAmount);
          this.previousAmountFailed = parseFloat(this.responsePrevious.data.failedAmount);
          this.previousTotal = this.previousAmountSuccess + this.previousAmountFailed / 100;
          this.totalCountPrevious = this.responsePrevious.data.transactionCount;
          this.previousCountFailed = this.responsePrevious.data.failedCount;
          this.previousCountSuccess = this.responsePrevious.data.successfulCount;

          this.previousPercentFailed = this.responsePrevious.data.failedPercent;
          this.previousPercentSuccess = this.responsePrevious.data.successfulPercent;

          this.percentChange = ((this.totalAmountCurrent - this.previousTotal) / this.previousTotal);
    
        }, error => {
          this.isData = false
          this.loading = false;
          console.log('cant get today response', error);
        })
    
  }


}
