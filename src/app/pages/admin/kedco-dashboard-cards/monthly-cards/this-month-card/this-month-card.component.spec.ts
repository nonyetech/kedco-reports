import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KedcoThisMonthCardComponent } from './this-month-card.component';

describe('ThisMonthCardComponent', () => {
  let component: KedcoThisMonthCardComponent;
  let fixture: ComponentFixture<KedcoThisMonthCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KedcoThisMonthCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KedcoThisMonthCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
