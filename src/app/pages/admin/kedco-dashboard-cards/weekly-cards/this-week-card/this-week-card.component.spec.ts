import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KedcoThisWeekCardComponent } from './this-week-card.component';

describe('ThisWeekCardComponent', () => {
  let component: KedcoThisWeekCardComponent;
  let fixture: ComponentFixture<KedcoThisWeekCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KedcoThisWeekCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KedcoThisWeekCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
