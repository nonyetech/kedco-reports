import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KedcoWeekCardComponent } from './week-card.component';

describe('KedcoWeekCardComponent', () => {
  let component: KedcoWeekCardComponent;
  let fixture: ComponentFixture<KedcoWeekCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KedcoWeekCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KedcoWeekCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
