import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';
import { DashboardSummary } from 'app/_models/DashboardSummary';

@Injectable({
    providedIn: 'root'
})

export class SummaryService {
    baseUrlV2 = 'http://197.253.19.76:6200/api/v1/kedco/transaction/dashboard';

    constructor(private httpClient: HttpClient) { }

// today Summary
    getToday(todayPayload: DashboardSummary, yesterdayPayload: DashboardSummary,): Observable<any[]> {               
        let getTodaySuccess = this.httpClient.post(this.baseUrlV2, todayPayload);
        let getTodayFailed = this.httpClient.post(this.baseUrlV2, yesterdayPayload);
        return forkJoin([getTodaySuccess, getTodayFailed]);
    }


     // this week
     getThisWeek(thisWeekPayload: DashboardSummary, lastWeekPayload: DashboardSummary): Observable<any[]> {
        let getThisWeekSuccess = this.httpClient.post(this.baseUrlV2, thisWeekPayload);
        let getThisWeekFailed = this.httpClient.post(this.baseUrlV2, lastWeekPayload);
        return forkJoin([getThisWeekSuccess, getThisWeekFailed]);
    }

    // this month
    getThisMonth(thisMonthPayload: DashboardSummary, lastMonthPayload: DashboardSummary): Observable<any[]> {
        let getThisMonthSuccess = this.httpClient.post(this.baseUrlV2, thisMonthPayload);
        let getThisMonthFailed = this.httpClient.post(this.baseUrlV2, lastMonthPayload);
        return forkJoin([getThisMonthSuccess, getThisMonthFailed]);
    }

}