import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})

export class CspService {

    constructor(private httpClient: HttpClient) { }
    // today channel
    getChannel(baseUrl): Observable<any> {
        return this.httpClient.get(baseUrl).pipe(map(response => {
            return  response;
        }));
    }

}