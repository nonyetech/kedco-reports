export class DashboardSummary {
    "category": string;
	"date": string;
	"region": string;
	"csp": string;
	"operator": string;
	"dateRange": string;
}