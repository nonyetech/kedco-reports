import { AdminDashboardComponent } from './pages/admin/admin-dashboard/admin-dashboard.component';
import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LoginComponent } from './_auth/login/login.component';
import { AuthGuard } from './_auth/auth.guard';
import { ServiceStatusComponent } from './pages/status/service-status/service-status.component';
import { BvnStatusComponent } from './pages/status/bvn-status/bvn-status.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { TransactionComponent } from './pages/Transaction/transaction/transaction.component';
import { TransactionReversalComponent } from './pages/Transaction/transaction-reversal/transaction-reversal.component';
import { TransactionRemittanceComponent } from './pages/Transaction/transaction-remittance/transaction-remittance.component';
import { SummaryReportComponent } from './pages/Transaction/summary-report/summary-report.component';
import { RegisterComponent } from './_auth/register/register.component';
import { RoleManagementComponent } from './pages/admin/role-management/role-management.component';
import { RoleGuard } from './_auth/role-guard.service';
import { DayCardComponent } from './pages/admin/dashboard-cards/daily-cards/day-card/day-card.component';
import { WeekCardComponent } from './pages/admin/dashboard-cards/weekly-cards/week-card/week-card.component';
import { MonthCardComponent } from './pages/admin/dashboard-cards/monthly-cards/month-card/month-card.component';
import { AgencyBankingComponent } from './AgencyBanking/agency-banking/agency-banking.component';
import { McashTransactionComponent } from './pages/Mcash/transaction/mcash-transaction.component';
import { McashComponent } from './pages/Mcash/terminals/mcash.component';
import { ErrorAnalysisComponent } from './pages/error-analysis/error-analysis.component';
import { KedcoDayCardComponent } from './pages/admin/kedco-dashboard-cards/daily-cards/day-card/day-card.component';
import { KedcoWeekCardComponent } from './pages/admin/kedco-dashboard-cards/weekly-cards/week-card/week-card.component';
import { KedcoMonthCardComponent } from './pages/admin/kedco-dashboard-cards/monthly-cards/month-card/month-card.component';


const routes: Route[] = [
  { path: 'login', component: LoginComponent },
  { path: 'not-found', component: NotFoundComponent },
  
  
  {
    path: '',
    // runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      // { path: 'admin-dashboard', component: AdminDashboardComponent, 
      // canActivate: [RoleGuard],data: { expectedRole: [3, 4, 5]} },

      // Agency Dashboard Route
      { path: 'admin-dashboard/day', component: DayCardComponent, 
      canActivate: [RoleGuard],data: { expectedRole: ['user','admin']} },

      { path: 'admin-dashboard/week', component: WeekCardComponent, 
      canActivate: [RoleGuard],data: { expectedRole: ['user','admin']} },

      { path: 'admin-dashboard/month', component: MonthCardComponent, 
      canActivate: [RoleGuard],data: { expectedRole: ['user','admin']} },

      // Kedco Dashboard Route
      { path: 'admin-dashboard/kedco/day', component: KedcoDayCardComponent, 
      canActivate: [RoleGuard],data: { expectedRole: ['user','admin']} },

      { path: 'admin-dashboard/kedco/week', component: KedcoWeekCardComponent, 
      canActivate: [RoleGuard],data: { expectedRole: ['user','admin']} },

      { path: 'admin-dashboard/kedco/month', component: KedcoMonthCardComponent, 
      canActivate: [RoleGuard],data: { expectedRole: ['user','admin']} },

      { path: 'transaction/details', component: TransactionComponent },
      { path: 'transaction/remittance', component: TransactionRemittanceComponent },
      { path: 'transaction/summary/report', component: SummaryReportComponent },

      
      { path: 'register', component: RegisterComponent,
       canActivate: [RoleGuard],data: { expectedRole: ['admin']}},

      { path: 'users', component: RoleManagementComponent,
      canActivate: [RoleGuard],data: { expectedRole: ['admin']}},

    ////////// MCASH ROUTE ////////////////////////
    //  { path: 'Mcash', component: McashComponent},
    ]
  },
  
  { path: '**', redirectTo: 'not-found', pathMatch: 'full' },

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
